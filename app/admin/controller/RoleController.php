<?php

namespace app\admin\controller;

use app\common\model\Role;
use app\common\model\PurviewRole;
use think\facade\Db;
use think\facade\View;

/**
 * Class 角色管理
 * @package app\admin\controller
 */
class RoleController extends AdminBase
{
    public function lists(Role $role)
    {
        if ($this->request->isAjax()) {
            $pageSize = $this->getPageSize();
            $page = $this->getPage();
            $data = Db::table(['t_purview' => 'p', 't_purview_role' => 'pr', 't_role' => 'r'])
                ->field(['r.role_id', 'r.role_name',  'GROUP_CONCAT(p.purview_name)' => 'purview_name'])
                ->where('p.purview_id=pr.purview_id AND pr.role_id=r.role_id')
                ->group('r.role_id')
                ->paginate($pageSize);
            $lists = $data->items();
            $total = $data->total();
            return $this->listJson($lists, $total);
        } else {
            return view();
        }
    }

    public function add(){

        if($this->request->isAjax()){
            $name = input('name');
            $purviewId = input('purviewId');
            if(empty($name) || empty($purviewId)) return $this->errJson('缺少参数');
            $info = Role::where('role_name', $name)->find();
            if(!empty($info)) return $this->errJson('角色已存在');
            $role = Role::create(['role_name' => $name]);
            $purviewId = trim($purviewId, ',');
            $purviewIds = explode(',', $purviewId);
            $data = [];
            foreach ($purviewIds as $key => $value){
                $data[] = ['role_id' => $role->role_id, 'purview_id' => $value];
            }
            $purviewRole = new PurviewRole();
            $pr = $purviewRole->saveAll($data, false);
            if($pr) {
                return $this->okJson('ok');
            } else {
                return $this->errJson('error');
            }

        } else {
            return view();
        }
    }

    public function edit()
    {
        $roleId = input('role_id');
        if($this->request->isAjax()){
            $name = input('name');
            $purviewId = input('purviewId');
            if(empty($name) || empty($purviewId) || empty($roleId)) return $this->errJson('缺少参数');
            $role = Role::where('role_id', $roleId)->find();

            if($role['role_name'] != $name){
                $roles = Role::where('role_name', $name)->find();
                if(!empty($roles)) return $this->errJson('角色已存在');
            }
            $role = Role::update(['role_name' => $name], ['role_id' => $roleId]);
            $purviewId = trim($purviewId, ',');
            $purviewIds = explode(',', $purviewId);
            $data = [];
            foreach ($purviewIds as $key => $value){
                $data[] = ['role_id' => $roleId, 'purview_id' => $value];
            }
            $purviewRole = new PurviewRole();
            $purviewRole->where('role_id', $roleId)->delete();
            $pr = $purviewRole->saveAll($data, false);
            if($pr) {
                return $this->okJson('ok');
            } else {
                return $this->errJson('error');
            }

        } else {
            $role = Role::find($roleId);
            $purviewRole = PurviewRole::where('role_id', $roleId)->select();
            View::assign('role', $role);
            View::assign('purviewRole', $purviewRole);
            return view();
        }
    }

    public function del()
    {
        $id = input('id');
        if(empty($id)) return $this->errJson('id不能为空');
        Role::destroy($id);
        return $this->okJson('ok');
    }
}