<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-6-13
 * Time: 下午2:20
 */

namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use app\common\model\Article;

class Task extends Command{

    protected function configure(){
        $this->setName('Task')->setDescription("更新活动状态");//这里的setName和php文件名一致,setDescription随意
    }

    /*
     * 报表-全局统计
     */
    protected function execute(Input $input, Output $output)
    {
        $param = ['cid' => 5, 'status' => 1];
        $data = Article::where($param)->field('aid, endtime')->select();
        foreach($data as $key => $value) {
            $time = $value['endtime'];
            if($time < time()) {
                Article::update(['status' => 2], ['aid' => $value['aid']]);
            }
        }
    }
}