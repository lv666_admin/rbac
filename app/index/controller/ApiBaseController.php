<?php
/**
 * User: lv
 * Date: 2019/5/29 10:30
 * Email: 578530370@qq.com
 */

namespace app\index\controller;

use app\common\controller\CommonController;
use app\common\model\AuthModel;


class ApiBaseController extends CommonController
{
    final public function initialize()
    {
        parent::initialize();
        $token = input('token');
        if(empty($token)) {
            echo json_encode(['code' => -1, 'msg' => 'token不能为空']);
            exit;
        }
        $info = $this->getInfo($token);
        if ( $info == false ) {
            echo json_encode(['code' => -1, 'msg' => 'token失效']);
            exit;
        }
        $this->uid = $info;
    }

    private function getInfo($token)
    {
        $info = AuthModel::where('token', $token)->field('token,uid')->find();
        if( empty($info) ) {
            return false;
        }
        return $info['uid'];
    }
}