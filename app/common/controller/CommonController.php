<?php

namespace app\common\controller;

use app\BaseController as Controller;

abstract class CommonController extends Controller
{
    public function initialize()
    {
        parent::initialize();
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Headers: X-Requested-With,X_Requested_With');
    }
    /**
     * 正确信息
     * @param string $msg
     * @param array $data
     * @param string $url
     * @return \think\response\Json
     */
    public function okJson($msg = '', $data = [], $url = '')
    {
        $json = ['code' => 0];
        if(!empty($msg)) $json['msg']  = $msg;
        if(!empty($url)) $json['url']  = $url;
        if(!empty($data)) $json['data']  = $data;
        return json($json);
    }

    /**
     * 错误信息
     * @param string $msg
     * @param string $url
     * @return \think\response\Json
     */
    public function errJson($msg = '', $url = '')
    {
        $json = [
            'code' => -1,
            'msg' => $msg,
            'url' => $url
        ];
        return json($json);
    }

    /**
     * 分页数据
     * @param array $data
     * @param int $total
     * @return \think\response\Json
     */
    public function
    listJson($data = [], $total = 0)
    {
        $json = [
            'code' => 0,
            'data' => $data,
            'count' => $total
        ];
        return json($json);
    }

    /**
     * 获取分页数量
     * @return int
     */
    public function getPageSize()
    {
        return $this->request->param('limit', 10);
    }

    /**
     * 获取请求页
     * @return int
     */
    public function getPage()
    {
        return $this->request->param('page\d', 1);
    }


    /**
     * 生成api token
     * @return string
     */
    public function setToken()
    {
        return md5(uniqid().time());
    }
}