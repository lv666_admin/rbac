<?php
/**
 * User: lv
 * Date: 2019/5/29 9:08
 * Email: 578530370@qq.com
 */

namespace app\index\controller;

use app\common\model\LinksModel;

use app\common\controller\CommonController;
use app\common\controller\SmsController;

class IndexController extends CommonController
{
    public function index()
    {
        $data = $this->request->post();
        return $this->okJson('ok', $data);
    }

    public function links()
    {
        $pageSize = $this->getPageSize();
        $where['status'] = 1;
        $data = LinksModel::where($where)
            ->field('lid, image, link')
            ->order('ctime desc')
            ->paginate($pageSize);
        $total = $data->total();
        $list = $data->items();
        return $this->listJson($list, $total);
    }

    public function sendSms()
    {
        $mobile = input('mobile');
        $result = $this->validate(['mobile' => $mobile], 'app\common\validate\MoblieValidate.mobile');
        if (true !== $result) {
            return $this->errJson($result);
        }
        $sms = new SmsController();
        return $sms::send($mobile);
    }
}