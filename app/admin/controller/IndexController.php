<?php

namespace app\admin\controller;

use app\common\model\Article;
use app\common\controller\SmsController;
use think\facade\View;
use think\facade\Session;

class IndexController extends AdminBase
{
    public function index()
    {
        if ($this->request->isAjax()) {
            $menuList = Session::get('menuList');
            return $this->listJson($menuList);
        }else{
            return view();
        }
    }

    public function main()
    {
//        $member = ArticleModel::where('cid', 5)->count();
//        $member1 = CourseModel::count();
//        $member2 = BusinessMemberModel::where('status', 1)->count();
        $data = [
            'member'  => 1,
            'member1' => 2,
            'member2' => 3
        ];
        View::assign('data', $data);
        return view();
    }

    public function bar()
    {
        $titles = ['直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎'];
        $data = [
            ['value'=> 335, 'name'=> '直接访问'],
            ['value'=> 310, 'name'=> '邮件营销'],
            ['value'=> 234, 'name'=> '联盟广告'],
            ['value'=> 135, 'name'=> '视频广告'],
            ['value'=> 1548, 'name'=> '搜索引擎']
        ];
        $data = [
            'title' => $titles,
            'data'  => $data
        ];
        return $this->listJson($data);
    }

    public function pie()
    {
        $titles = ['餐饮', '美容美发', '家电维修'];
        $values =  [10, 52, 200];
        $data = [
            'title'  => $titles,
            'value' => $values
        ];
        return $this->listJson($data);
    }

    public function sendSms()
    {
        $mobile = input('mobile');
        $sms = new SmsController();
        return $sms::send($mobile);
    }

    public function delImg()
    {
        $path = input('path');
        if(empty($path)) return $this->errJson('路径不能为空');
        if(!file_exists(ROOT.$path)) return $this->errJson('文件不存在');
        unlink(ROOT.$path);
        return $this->okJson('ok');
    }
}