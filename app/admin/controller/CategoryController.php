<?php
/**
 * User: lv
 * Date: 2018/12/6 17:00
 * Email: 578530370@qq.com
 */

namespace app\admin\controller;

use app\common\model\Category;
use think\facade\Request;
use think\facade\View;


class CategoryController extends AdminBase
{
    public function lists(Category $cate)
    {
        if(Request::isAjax()) {
            $keyword = input('post.key');
            $where = [];
            $keyword && $where[] = ['cate_name', 'like', "%$keyword%"];
            $data = $cate
                ->where($where)
                ->order('cate_priority')
                ->select()
                ->toArray();
            $lists = self::combineCate($data);
            $total = count($lists);
            return $this->listJson($lists, $total);
        }else{
            return view();
        }
    }

    public function add(Category $cate)
    {
        if(Request::isAjax()) {
            $data = Request::post();
            $cate = $cate->saveCate($data);
            if ($cate) {
                return $this->okJson('添加成功');
            } else {
                return $this->errJson('添加失败');
            }
        }else{
            $cate = Category::select()->toArray();
            $lists = self::combineCate($cate);
            View::assign('lists',$lists);
            return view();
        }
    }

    public function edit()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $cid = $data['cid'];
            unset($data['cid']);
            if(empty($cid)) return $this->errJson('缺少参数');
            $cate = Category::update($data, ['cate_id' => $cid]);
            if ($cate) {
                return $this->okJson('修改成功');
            } else {
                return $this->errJson('修改失败');
            }
        } else {
            $cid = input('cid');
            if(empty($cid)) return $this->errJson('缺少参数');
            $data = Category::find($cid);

            $cate = Category::select()->toArray();
            $lists = self::combineCate($cate);
            View::assign('lists',$lists);
            View::assign('data', $data);
            return View::fetch();
        }
    }

    public function del()
    {
        $cid = input('cid');
        if(empty($cid)) return $this->errJson('缺少参数');
        $cate = Category::destroy($cid);
        if ($cate) {
            return $this->okJson('删除成功');
        } else {
            return $this->errJson('删除失败');
        }
    }

    //修改优先级
    public function editPriority()
    {
        $id = input('cid');
        $value = input('value');
        $field = input('field');
        $cate = Category::update([$field => $value], ['cate_id' => $id]);
        if ($cate) {
            return $this->okJson('修改成功');
        } else {
            return $this->errJson('修改失败');
        }
    }

    //设置是否为推荐
    public function editRecommend()
    {
        $id = input('cid');
        $value = input('value');
        if($value == 0) {
            $value = 1;
        }else if($value ==1 ){
            $value = 0;
        }
        $cate = Category::update(['cate_recommend' => $value], ['cate_id' => $id]);
        if ($cate) {
            return $this->okJson('修改成功');
        } else {
            return $this->errJson('修改失败');
        }
    }


}