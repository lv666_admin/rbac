<?php

namespace app\admin\controller;

use app\common\model\Category;
use app\common\model\Article;
use think\facade\Request;
use utils\Upload;
use utils\Ueditor;
use think\facade\View;

class ArticleController extends AdminBase
{
    public function lists(Article $articles)
    {
        if ($this->request->isAjax()) {
            $keyword=input('post.key');
            $type=input('post.type');
            $pageSize = $this->getPageSize();
            $where = [];
            $keyword && $where[] = ['title', 'like', "%$keyword%"];
            $type && $where[] = ['cid', '=', "$type"];
            $data = $articles
                ->where($where)
                ->order('aid desc')
                ->paginate($pageSize);
            foreach ($data as $key=>$value){
                $cate = Category::where(['cate_id' => $value['cid']])->field('cate_name')->find();
                $data[$key]['cate_name'] =$cate['cate_name'];
                $data[$key]['utime'] = date('Y-m-d H:i',$value['utime']);
            }
            $total = $data->total();
            $list = $data->items();
            return $this->listJson($list, $total);
        } else {
            $cate = $this->articleCate();
            $type = self::combineCate($cate);
            View::assign('type',$type);
            return view();
        }
    }

    public function add(Article $articles)
    {
        if (Request::isAjax()) {
            $data = Request::post();
            if($data['endtime']) $data['endtime'] = strtotime($data['endtime']);
            $user = $this->app->user;
            $data['auther'] = $user['uid'];
            //处理百度编辑器图片
            $data['content'] = Ueditor::add($data['content']);
            $article = $articles->addArticle($data);
            if ($article['code'] == 0) {
                return $this->okJson('success');
            } else {
                return $this->errJson($article['msg']);
            }
        } else {
            $cate = $this->articleCate();
            $type = self::combineCate($cate);
            View::assign('type',$type);
            return View::fetch();
        }
    }

    public function delete(Article $articles)
    {
        $id = input('aid');
        $article = $articles->where(['aid'=>$id])->field('image,content')->find();
        if($article['image'] != '') Upload::unlink($article['image']);
        if($article['content'] != '') Ueditor::del($article['content']);

        Article::destroy($id);
    }

    public function edit()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $aid = $data['aid'];
            if($data['endtime']) $data['endtime'] = strtotime($data['endtime']);

            $info = Article::field('content')->find($aid);
            $data['content'] = Ueditor::edit($data['content'],$info['content']);

            unset($data['aid']);
            $data['utime'] = time();
            $article = Article::update($data, ['aid' => $aid]);
            if ($article) {
                return $this->okJson('success');
            } else {
                return $this->errJson($article['msg']);
            }
        } else {
            $aid = input('aid');
            $data = Article::find($aid);
            if($data['endtime']) $data['endtime'] = date('Y-m-d', $data['endtime']);
            View::assign('data', $data);

            $cate = $this->articleCate();
            $type = self::combineCate($cate);
            View::assign('type',$type);
            return View::fetch();
        }
    }

    private function articleCate()
    {
        $cate = Category::where('cate_id', '1')->field('cate_id')->find()->toArray();
        $cids = Category::where('cate_pid', $cate['cate_id'])->field('cate_id')->select();
        $ids = '';
        foreach ($cids as $key => $value) {
            $ids .= $value['cate_id'].',';
        }
        $ids = $ids . $cate['cate_id'];
        return Category::whereIn('cate_id', $ids)->select()->toArray();
    }
}