# rbac工作流基础框架

## 介绍
### 系统集成rbac、工作流和文章管理等功能。功能如下图所示：

![登录](public/static/images/logo.png)

![首页](public/static/images/index.png)

![输入图片说明](public/static/images/%E4%B8%9A%E5%8A%A1%E5%88%97%E8%A1%A8.png)

![输入图片说明](public/static/%E4%B8%9A%E5%8A%A1%E5%AE%A1%E6%89%B9.png)

![输入图片说明](public/static/images/%E4%B8%9A%E5%8A%A1%E5%AE%A1%E6%89%B92.png)

![输入图片说明](public/static/images/%E4%B8%9A%E5%8A%A1%E5%AE%A1%E6%89%B93.png)

![输入图片说明](public/static/images/%E4%B8%9A%E5%8A%A1%E5%AE%A1%E6%89%B95.png)

![流程列表](public/static/images/%E6%B5%81%E7%A8%8B%E8%AE%BE%E8%AE%A11.png)

![流程信息编辑](public/static/images/%E6%B5%81%E7%A8%8B%E4%BF%AE%E6%94%B9.png)

![流程设计](public/static/images/%E6%B5%81%E7%A8%8B%E8%AE%BE%E8%AE%A1.png)

![菜单管理](public/static/images/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86.png)

![权限管理](public/static/images/%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86.png)

![角色管理](public/static/images/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.png)

![用户管理](public/static/images/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png)

## 软件架构
1. php7.4
2. thinkphp6.0
3. mysql5.6
4. JavaScript
5. layui
6. tpflow5.0


## 安装教程

1.  搭建php运行环境
2.  将项目放到web服务器指定目录上（apache、nginx、iis）
3.  重启web服务器
2.  在浏览器运行:[http://127.0.0.1:81/admin](http://127.0.0.1:81/admin)(如果配置端口，记得在IP后加端口)

## 参考文档

1.  [tp6.0](https://www.kancloud.cn/manual/thinkphp6_0/1037479)
2.  [tpflow5.0](https://gadmin8.com/index/product.html)
