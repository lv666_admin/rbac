<?php

return [
    // auth配置
    'auth_on' => true, // 权限开关
    'auth_cache' => true, //是否开启缓存
    'auth_key' => '_auth_', // 数据缓存的key
    'auth' => 'purview', // 权限表
    'auth_role' => 'purview_role', // 权限角色对应表
    'role' => 'role', // 角色表
    'role_user' => 'user_role', // 用户角色对应表
    'users' => 'users', // 用户信息表
    'users_auth_fields' => '',//用户需要验证的规则表达式字段 空代表所有用户字段

    //不需要登录的
    'no_need_login_url' => [
        '/admin/login/login'
    ],

    //登录用户不需要验证的
    'allow_visit' => [
        '/admin/index/index',
        '/admin/index/main',
        '/admin/login/logout'
    ],
    'login_path' => url('/admin/login/login')
];