<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-1-24
 * Time: 上午11:39
 */

namespace app\common\model;

use think\Model;

class Code extends Model
{
    static public function add($mobile, $code)
    {
        if(empty($mobile) || empty($code)) {
            return false;
        }
        $detail = Code::where(['mobile' => $mobile])->find();
        if(!empty($detail)) {
            Code::where(['mobile'=>$mobile])->delete();
        }
        $data = [
            'mobile' => $mobile,
            'code'   => $code,
            'utime'  => time()
        ];
        return Code::create($data);
    }

    static public function get($mobile)
    {
        $details = Code::where(['mobile' => $mobile])->find();
        return $details['code'];
    }
}