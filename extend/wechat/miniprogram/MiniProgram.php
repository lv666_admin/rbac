<?php

namespace wechat\miniprogram;

include __DIR__ . '/lib/wxBizDataCrypt.php';

class MiniProgram
{
    private $pc = null;

    public function __construct($appId, $sessionKey)
    {
        if (!$appId || !$sessionKey) {
            throw new \Exception('appId and sessionKey is empty!!');
        }

        $this->pc = new \WXBizDataCrypt($appId, $sessionKey);
    }

    public function decryptData($encryptedData, $iv)
    {
        $data = null;
        $errCode = $this->pc->decryptData($encryptedData, $iv, $data);
        if ($errCode == 0) {
            return $data;
        } else {
            throw new \Exception("errCode is {$errCode}");
        }
    }
}