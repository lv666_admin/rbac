<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-6-26
 * Time: 上午10:35
 */

namespace app\admin\controller;

use app\common\model\Purview;
use think\facade\View;


/**
 * Class 权限管理
 * @package app\admin\controller
 */
class PurviewController extends AdminBase
{
    public function lists(Purview $purview)
    {
        if ($this->request->isAjax()) {
            $data = $purview->select();
            return $this->listJson($data);
        } else {
            return view();
        }
    }

    public function add()
    {
        $param = $this->request->post();
        $result = $this->validate($param, 'app\common\validate\PurviewValidate.add');
        if (true !== $result) {
            return $this->errJson($result);
        }
        $info = Purview::where('purview_route', $param['purview_route'])->find();
        if(!empty($info)) return $this->errJson('url已存在');
        Purview::create($param);
        return $this->okJson('ok');
    }

    public function edit()
    {
        $param = $this->request->post();
        $result = $this->validate($param, 'app\common\validate\PurviewValidate.edit');
        if (true !== $result) {
            return $this->errJson($result);
        }
        Purview::update($param,['purview_id' => $param['purview_id']]);
        return $this->okJson('ok');
    }

    public function del()
    {
        $id = input('id');
        if(empty($id)) return $this->errJson('id不能为空');
        Purview::destroy($id);
        return $this->okJson('ok');
    }
}