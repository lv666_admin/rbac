<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-1-25
 * Time: 上午10:43
 */

namespace app\admin\controller;

use app\common\model\Video;
use app\common\model\Category;
use think\facade\Request;
use utils\Upload;
use think\facade\View;

class VideoController extends AdminBase
{
    public function lists()
    {
        if ($this->request->isAjax()) {
            $keyword = input('post.key');
            $type    = input('post.type');
            $pageSize = $this->getPageSize();
            $where = [];
            if(!empty($keyword) && is_numeric($keyword) && strlen($keyword) == 11){
                $keyword && $where[] = '';
            }else{
                $keyword && $where[] = ['title', 'like', "%$keyword%"];
            }
            $type && $where[] = ['cid', '=', "$type"];
            $data = Video::where($where)
                ->order('utime desc')
                ->paginate($pageSize);
            foreach ($data as $key=>$value){
                $cate = Category::where(['cate_id' => $value['cid']])->field('cate_name')->find();
                $data[$key]['cid'] =$cate['cate_name'];
            }
            $total = $data->total();
            $list = $data->items();
            return $this->listJson($list, $total);
        } else {
            return view();
        }
    }

    public function add(Video $video)
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $data['pusher'] = $this->app->user->uid;
            if ($video->addVideo($data)){
                return $this->okJson('保存成功');
            }else{
                return $this->errJson('标题已存在');
            }
        } else {
            $type = Category::select()->toArray();
            $type = self::combineCate($type);
            View::assign('type',$type);
            return view();
        }
    }

    public function edit()
    {
        if (Request::isAjax()) {
            $param = Request::post();
            $id = $param['id'];
            $data = Video::find($id);
            if($param['img'] != $data['img'] || $param['address'] != $data['address']){
                Upload::unlink($data['img']);
                Upload::unlink($data['address']);
            }

            unset($param['id']);
            $param['video_utime'] = date("Y-m-d H:i");
            $video = Video::update($param, ['id' => $id]);
            if ($video) {
                return $this->okJson('修改成功');
            } else {
                return $this->errJson('修改失败');
            }
        } else {
            $id = input('id');
            $data = Video::find($id);
            View::assign('data', $data);

            $type = Category::select()->toArray();
            $type = self::combineCate($type);
            View::assign('type',$type);
            return View::fetch();
        }
    }

    public function del()
    {
        $id = input('id');
        $video = Video::where(['id'=>$id])->field('img, address')->find();
        if($video['img'] != '') {
            Upload::unlink($video['img']);
            Upload::unlink($video['address']);
        }
        $isDel = Video::destroy($id);
        if($isDel) {
            return $this->okJson('删除成功');
        }else {
            return $this->errJson('删除失败');
        }


    }
}