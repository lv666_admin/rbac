<?php

namespace app\admin\controller;


use utils\Upload;
use think\facade\View;

class UploadController extends AdminBase
{
    public function upfile()
    {
        header("Content-Type: text/html;charset=utf-8");
        $type = 'original';
        $files = Upload::getFiles($type);
        $result = [];
        foreach ($files as $file) {
            $image = \think\Image::open('./uploads/'.$file);
            $thumbPath = './uploads/thumb'.trim($file, $type);
            $dir = substr($thumbPath,0,strrpos($thumbPath,DIRECTORY_SEPARATOR));
            if (!file_exists($dir)) {
                @mkdir($dir, 0777, true);
            }
            $image->thumb(150, 150)->save($thumbPath);

            $result[] = trim($thumbPath, '.');
        }
        return $this->okJson('上传成功', $result);
    }

    public function upVideo()
    {
        $video = Upload::getVideo();
        if($video) {
            return $this->okJson('上传成功', $video);
        }else{
            return $this->errJson('上传中', $video);
        }
    }

    public function del()
    {
        $path = input('path');
        if(empty($path)) return $this->errJson('路径不能为空');
        $res = Upload::unlink($path);
        return $this->okJson($res);
    }
}