<?php

namespace app\http\middleware;

use think\Request;
use security\Auth;

class AdminMiddle
{
    /**
     * @var Auth
     */
    static $Auth = null;
    public function handle(Request $request, \Closure $next)
    {
        // 注入用户
        app()->auth = Auth::getInstance();
        app()->user = Auth::getInstance()->getUserInfo();

        if (!Auth::getInstance()->notNeedLogin()) {

            if (!Auth::getInstance()->isLogin()) {
                return redirect('/admin/login/login');
            }

            if (!Auth::getInstance()->check()) {
                return json(['code' => -1, 'msg' => '没有权限操作']);
            }
        }
        return $next($request);
    }
}