<?php

namespace app\common\model;

use think\Model;

class Article extends Model
{
     protected $pk = 'aid';

     protected $autoWriteTimestamp = false;

     public function addArticle($data){
         $data['ctime'] = time();
         $data['utime'] = time();
         if(!$this->where(['title' => $data['title']])->count()) {
             Article::create($data);
             return ['code'=>0,'msg'=>'添加成功'];
         }
         return ['code'=>-1,'msg'=>'标题已存在'];

     }

     public function lists(array $where = [], string $field, int $limit, string $order)
     {
         $lists = Article::where($where)->field($field)->limit($limit)->order($order)->select();
         return $lists;
     }

}