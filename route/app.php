<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');

Route::group('wf', function () {
    Route::rule('designapi', '\tpflow\Api@designapi');//工作流设计统一接口API
    Route::rule('wfapi', '\tpflow\Api@wfapi');//工作流前端管理统一接口
    Route::rule('wfdo', '\tpflow\Api@wfdo');//工作流审批统一接口
    Route::rule('wfAccess', '\tpflow\Api@wfAccess');//数前端权限控制中心接口
    Route::rule('wfUserData', '\tpflow\Api@wfUserData');//数据接口
});