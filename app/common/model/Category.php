<?php

namespace app\common\model;

use think\Model;

class Category extends Model
{
    protected $pk = 'cate_id';

    public function saveCate($data){
        $data['cate_name'] && $param['cate_name'] = $data['cate_name'];
        $data['cate_pid'] && $param['cate_pid'] = $data['cate_pid'];
        $data['cate_url'] && $param['cate_url'] = $data['cate_url'];
        $data['cate_status'] && $param['cate_status'] = $data['cate_status'];
        $data['cate_priority'] && $param['cate_priority'] = $data['cate_priority'];
        $data['cate_recommend'] && $param['cate_recommend'] = $data['cate_recommend'];
        $param['cate_ctime'] = date('Y-m-d H:i:s');
        $cate = Category::where(['cate_id' => $data['cate_pid']])->field('cate_depth')->find();
        if(empty($cate)){
            $param['cate_depth'] = 0;
        }else{
            $param['cate_depth'] = $cate['cate_depth'] + 1;
        }
        if(!$this->where(['cate_name' => $data['cate_name']])->count()) {
            Category::create($param);
            return true;
        }
        return false;
    }

}