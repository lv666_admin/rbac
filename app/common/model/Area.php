<?php

namespace app\common\model;

use think\Model;

class Area extends Model
{

    public function getList()
    {
        $field = 'id, title, type';
        $lists = Area::where('status', 1)->field($field)->select();
        return $lists;
    }

    public function getArea($areaid, $full = true)
    {
        $province = substr($areaid, 0, 2) . '0000';
        $city = substr($areaid, 0, 4) . '00';
        $area = array($province, $city, $areaid);
        if (!$full)
            unset($area[0]);
        $areas = '';
        foreach ($area as $key => $value) {
            $area = $this->get($value, array('title'));
            $areas .= $area['title'] . '/';
        }
        if (!$full) {
            $areas = str_replace('市', '', str_replace('县', '', $areas));
        }
        return trim($areas, '/');
    }


    public static function getTopCodes($code) {
        $code = "$code";
        $code0 = substr($code, 0, 2) . '0000';
        $code1 = substr($code, 0, 4) . '00';
        return array($code0, $code1, $code);
    }

}