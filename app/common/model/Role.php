<?php
/**
 * User: lv
 * Date: 2019-6-24 23:02
 * Email: 578530370@qq.com
 */

namespace app\common\model;

use think\Model;

class Role extends Model
{
    protected $pk = 'role_id';

    public function purviewRole()
    {
        return $this->belongsToMany('PurviewModel');
    }
}