<?php
namespace aliyun;

class Signature{
    public $data;
    public $url;

    public function __construct($actionArray, $url){
        $this->accessKeySecret = config('aliyun.accessSecret');
        $this->url = $url;
        date_default_timezone_set("GMT");
        $this->data = array(
            // 公共参数
            'Format' => 'JSON',
            'Version' => '2017-03-21',
            'AccessKeyId' => config('aliyun.accessKeyId'),
            'SignatureVersion' => '1.0',
            'SignatureMethod' => 'HMAC-SHA1',
            'SignatureNonce'=> uniqid(),
            'Timestamp' => date('Y-m-d\TH:i:s\Z'),
        );
        //判断输入的参数是否为数组
        if(is_array($actionArray)){
            $this->data = array_merge($this->data,$actionArray);
        }
//        dump($this->data);exit;
    }

    public function percentEncode($str)
    {
        // 使用urlencode编码后，将"+","*","%7E"做替换即满足ECS API规定的编码规范
        $res = urlencode($str);
        $res = str_replace(array('+', '*'), array('%20', '%2A'), $res);
        $res = preg_replace('/%7E/', '~', $res);
        return $res;
    }

    public function computeSignature($parameters, $accessKeySecret)
    {
        // 将参数Key按字典顺序排序
        ksort($parameters);
        // 生成规范化请求字符串
        $canonicalizedQueryString = '';
        foreach($parameters as $key => $value)
        {
            $canonicalizedQueryString .= '&' . $this->percentEncode($key)
                . '=' . $this->percentEncode($value);
        }
        // 生成用于计算签名的字符串 stringToSign
        $stringToSign = 'GET&%2F&' . $this->percentencode(substr($canonicalizedQueryString, 1));
        // 计算签名，注意accessKeySecret后面要加上字符'&'
        $signature = base64_encode(hash_hmac('sha1', $stringToSign, $accessKeySecret . '&', true));
        return $signature;
    }

    public function callInterface(){
        // 计算签名并把签名结果加入请求参数
        $this->data['Signature'] = $this->computeSignature($this->data, $this->accessKeySecret);
        // 发送请求
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . http_build_query($this->data));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        $res = json_decode($res);
        return $res;
    }
}