<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-1-22
 * Time: 下午4:01
 */

namespace app\common\model;

use think\Model;

class Video extends Model
{
    protected $pk = 'id';

    public function addVideo($data){
        if(!$this->where(['title' => $data['title']])->count()) {
            Video::create($data);
            return true;
        }
        return false;
    }
}