<?php

namespace utils;

class Ueditor
{
    static public function add($content)
    {
        if(!empty($content))
        {
            $pattern = '/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png]))[\'|\"].*?[\/]?>/i';
            preg_match_all($pattern,$content,$res);
            $num = count($res[1]);
            if($num == 0) return $content;
            for($i = 0; $i < $num; $i++)
            {
                $ueditor_img = $res[1][$i];
                //新建日期文件夹
                $tmp_arr = explode('/',$ueditor_img);

                $upDir = DIRECTORY_SEPARATOR .'ueditor' .DIRECTORY_SEPARATOR;
                if (!file_exists(ROOT.DIRECTORY_SEPARATOR .'uploads'. $upDir)) {
                    @mkdir(ROOT.DIRECTORY_SEPARATOR .'uploads'. $upDir, 0777);
                }

                $datefloder = ROOT.'/uploads/ueditor/'.$tmp_arr[3];
                if(!is_dir($datefloder))
                {
                    @mkdir($datefloder, 0777);
                }
                $tmpimg = '.'.$ueditor_img;
                $newimg = str_replace('/image/','/ueditor/',$tmpimg);
                //转移图片
                rename($tmpimg, $newimg);
            }
            return $content = str_replace('/image/','/ueditor/',$content);
        }
    }

    static public function edit($content, $oldContent)
    {
        if(!empty($content))
        {
            //正则表达式匹配查找图片路径
            $pattern = '/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png]))[\'|\"].*?[\/]?>/i';
            preg_match_all($pattern,$content,$res);
            $num = count($res[1]);
            if($num == 0) return $content;
            $imgarr = [];
            for($i=0;$i<$num;$i++)
            {
                $ueditor_img = $res[1][$i];
                //判断是否是新上传的图片
                $pos = stripos($ueditor_img,"/image/");
                if($pos != false)
                {
                    $upDir = DIRECTORY_SEPARATOR .'ueditor' .DIRECTORY_SEPARATOR;
                    if (!file_exists(ROOT.DIRECTORY_SEPARATOR .'uploads'. $upDir)) {
                        @mkdir(ROOT.DIRECTORY_SEPARATOR .'uploads'. $upDir, 0777);
                    }
                    //新建日期文件夹
                    $tmp_arr = explode('/',$ueditor_img);
                    $datefloder = ROOT.'/uploads/ueditor/'.$tmp_arr[3];
                    if(!is_dir($datefloder))
                    {
                        @mkdir($datefloder, 0777);
                    }
                    $tmpimg = '.'.$ueditor_img;
                    $newimg = str_replace('/image/','/ueditor/',$tmpimg);
                    //转移图片
                    rename($tmpimg, $newimg);
                }else {
                    $imgarr[]=$ueditor_img;
                }
            }

            if(!empty($oldContent)) {
                $pattern = '/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png]))[\'|\"].*?[\/]?>/i';
                preg_match_all($pattern,$oldContent,$oldRes);
                $num = count($oldRes[1]);
                if($num == 0) return null;
                foreach ($oldRes[1] as $key => $value) {
                    if(!in_array($value,$imgarr)){
                        if(file_exists(ROOT.$value)) {
                            unlink(ROOT.$value);
                        }
                    }
                }

            }
            return $content = str_replace('/image/','/ueditor/',$content);
        }
    }

    //删除在编辑时被删除的原有图片
    static public function del($oldContent)
    {
        if(!empty($oldContent))
        {
            //正则表达式匹配查找图片路径
            $pattern = '/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png]))[\'|\"].*?[\/]?>/i';
            preg_match_all($pattern, $oldContent, $oldres);
            $num = count($oldres[1]);
            if($num == 0) return $oldContent;
            for($i = 0; $i < $num; $i++)
            {
                $delimg = $oldres[1][$i];
                if(file_exists(ROOT.$delimg)) {
                    unlink(ROOT.$delimg);
                }
            }
        }
    }

}