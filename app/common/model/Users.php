<?php

namespace app\common\model;

use security\Auth;
use think\Model;

class Users extends Model
{
    protected $pk = 'uid';

    /**
     * 登陆
     * @param $username
     * @param $password
     * @return bool
     * @throws
     */
    public function login($username, $password)
    {
        $user = $this->where([
            'username' => $username,
            'status' => 1
        ])->find();

        if ($user && password_verify($password, $user->password )) {
            unset($user->password);
            return Auth::getInstance()->login($user);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function logout()
    {
        return Auth::getInstance()->logout();
    }

    /**
     * @return mixed
     */
    public function getUserInfo()
    {
        return Auth::getInstance()->getUserInfo();
    }
}