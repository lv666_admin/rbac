<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-7-11
 * Time: 上午9:36
 */

namespace app\admin\controller;

use app\common\model\Menu;
use app\common\model\MenuRole;
use app\common\model\Role;
use think\facade\Db;
use think\facade\View;

class MenuController extends AdminBase
{
    public function lists()
    {
        if ($this->request->isAjax()) {
            $data = Db::table('t_menu m')
                ->field(['m.menu_id', 'm.menu_name', 'm.menu_pid', 'm.menu_sort', 'GROUP_CONCAT(r.role_name)' => 'role_name'])
                ->leftJoin('t_menu_role mr', 'm.menu_id = mr.menu_id')
                ->leftJoin('t_role r', 'mr.role_id = r.role_id')
                ->group('m.menu_id')
                ->order('menu_sort')
                ->select()->toArray();
            $lists = self::combineMenu($data);
            $total = count($lists);
            return $this->listJson($lists, $total);
        } else {
            return view();
        }
    }

    public function add()
    {
        if($this->request->isAjax()){
            $menuData = $this->request->post();
            if(empty($menuData['menu_name'])) return $this->errJson('缺少参数');
            //用户名是否重复
            $menu = Menu::where('menu_name','=',$menuData['menu_name'])->find();
            if(isset($menu)) return $this->errJson('名称已存在,请重新输入');

            $res = Menu::create($menuData);

            if(!empty($menuData['roleId'])) {
                $roleIds = explode(',', $menuData['roleId']);
                $data = [];
                foreach ($roleIds as $key => $value){
                    $data[] = ['menu_id' => $res->menu_id, 'role_id' => $value];
                }
                $menuRole = new MenuRole();
                $menuRole->saveAll($data, false);
            }

            if(isset($res)){
                return $this->okJson('添加成功');
            }else{
                return $this->errJson('添加失败');
            }

        } else {
            $menu = Menu::select()->toArray();
            $lists = self::combineMenu($menu);
            View::assign('lists', $lists);
            $role = Role::select();
            View::assign('roles', $role);
            return view();
        }
    }

    public function edit()
    {
        $id = input('menu_id');
        if($this->request->isAjax()){
            $param = $this->request->post();
            unset($param['menu_id']);
            if(empty($param['menu_name']) || empty($id)) return $this->errJson('缺少参数');
            $menu = Menu::where('menu_id', $id)->find();

            if($menu['menu_name'] != $param['menu_name']){
                $menus = Menu::where('menu_name', $param['menu_name'])->find();
                if(!empty($menus)) return $this->errJson('名称已存在');
            }
            $res = Menu::update($param, ['menu_id' => $id]);

            $menuRole = new MenuRole();
            $menuRole->where('menu_id', $id)->delete();
            if(!empty($param['roleId'])) {
                $roleId = trim($param['roleId'], ',');
                $roleIds = explode(',', $roleId);
                $data = [];
                foreach ($roleIds as $key => $value) {
                    $data[] = ['menu_id' => $id, 'role_id' => $value];
                }
                $menuRole->saveAll($data, false);
            }
            if($res) {
                return $this->okJson('ok');
            } else {
                return $this->errJson('error');
            }

        } else {
            $menu = Menu::find($id);
            $menus = Menu::select()->toArray();
            $menus = self::combineMenu($menus);
            $role = Role::select();
            $myRoles = MenuRole::where('menu_id', $id)->field('role_id')->select()->toArray();
            $myRoles = array_column($myRoles, 'role_id');
            View::assign('menu',$menu);
            View::assign('roles', $role);
            View::assign('menus', $menus);
            View::assign('myRoles', $myRoles);
            return view();
        }
    }

    public function del()
    {
        $id = input('id');
        if(empty($id)) return $this->errJson('id不能为空');
        Menu::destroy($id);
        return $this->okJson('ok');
    }

    public function editSort()
    {
        $id = input('id');
        $value = input('value');
        $sort = Menu::update(['menu_sort' => $value], ['menu_id' => $id]);
        if ($sort) {
            return $this->okJson('修改成功');
        } else {
            return $this->errJson('修改失败');
        }
    }
}