<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19-6-3
 * Time: 上午11:59
 */

namespace app\common\validate;

use think\Validate;

class PurviewValidate extends Validate
{
    protected $rule =   [
        'purview_id' => 'require|integer',
        'purview_name' => 'require|chsAlpha',
        'purview_route'  => 'require'
    ];

    protected $message  =   [
        'purview_id.integer'    => 'id格式错误',
        'purview_name.chsAlpha' => '名称格式错误',
        'purview_route.require' => '路由不能为空'
    ];

     protected $scene = [
         'add'   =>  ['purview_name', 'purview_route'],
         'edit'  =>  ['purview_id', 'purview_name', 'purview_route'],
     ];
}