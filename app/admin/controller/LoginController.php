<?php

namespace app\admin\controller;

use app\common\controller\CommonController;
use app\common\model\Users;
use think\facade\View;

class LoginController extends CommonController
{
    /**
     * @return \think\response\Json|\think\response\View
     * @throws
     */
    public function login(Users $users)
    {
        if ($this->request->isAjax()) {
            $username = $this->request->param('username', '');
            $password = $this->request->param('password', '');

            if ($users->login($username, $password)) {
                return $this->okJson('登陆成功', '', '/admin');
            } else {
                return $this->errJson('登陆失败');
            }
        } else {
            return view();
        }
    }

    /**
     * @param Users $users
     * @return \think\response\Json
     */
    public function logout(Users $users)
    {
        $users->logout();
        return $this->okJson('退出成功');
    }
}