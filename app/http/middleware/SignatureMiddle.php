<?php
/**
 * User: lv
 * Date: 2019/5/30 8:29
 * Email: 578530370@qq.com
 */

namespace app\http\middleware;

use app\common\controller\CommonController;

class SignatureMiddle extends CommonController
{
    const TOKEN = 'JIAYI';
    public function handle($request, \Closure $next)
    {
//        $str = $request->param('str');
//        $signature = $request->param('signature');
//        if ( $this->arithmetic($str) != $signature ) {
//            return json(['code' => -1, 'msg' => '签名失败']);
//        }
        return $next($request);
    }

    /**
     * @param $str 随机字符串
     * @return string 返回签名
     */
    private function arithmetic($str){
        $str = $str.self::TOKEN;
        $signature = base64_encode(md5($str));
        return $signature;
    }
}