<?php
/**
 * User: lv
 * Date: 2019/5/30 15:39
 * Email: 578530370@qq.com
 */

namespace app\common\validate;

use think\Validate;

class MoblieValidate extends Validate
{
    protected $rule =   [
        'mobile' => 'require|mobile',
        'code' => 'require|number|length:4',
    ];

    protected $message  =   [
        'mobile.require'    => '手机号不能为空',
        'mobile.mobile'     => '手机号格式错误',
        'code.require'      => '验证码不能为空',
        'code.number'       => '验证码格式不对',
        'code.length'       => '验证码格式不对',
    ];

    //场景验证
    protected $scene = [
        //手机号数据验证
        'mobile'  =>  ['mobile'],
        //登录数据验证
        'login' => ['mobile', 'code']
    ];
}