/** layuiAdmin.std-v1.2.0 LPPL License By http://www.layui.com/admin/ */
;layui.define(function (e) {
    var i = (layui.$, layui.layer, layui.laytpl, layui.setter, layui.view, layui.admin);
    i.events.logout = function () {
        console.log(layui.setter.base);
        i.req({
            url: "/admin/login/logout", type: "get", data: {}, done: function (e) {
                i.exit(function () {
                    location.href = "/admin/login/login.html"
                })
            }
        })
    }, e("common", {})
});